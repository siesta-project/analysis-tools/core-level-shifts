!This program calculates de core level chemical shifts "Initial shifts" for 
!any given core level of one chosen atom. Calculating the matrix 
!elements of <Psi|V|Psi> where V is the hartree, exchange and correlation potentials
!created by the valence electrons in a pseudopotential calculation.
!And following the Physical Review B,Vol 50,N 11 by X.Blase et al.

      PROGRAM PRINCIPAL
 
      USE precision
      USE dosiesta 
      USE doabinit 

        IMPLICIT none
        INTEGER ::deg
        CHARACTER:: sysname*70, programa*75 
        integer ::lmax,nsp,nspin
        real(dp)::integral,potential
        INTEGER::ierr,choosatom,factorx,factory,factorz
        CHARACTER(LEN=10)::choosedeg,chooseae1,chooseae2
        INTEGER::nskip

! ---------------------------------------------------------------------------
! Reads the input file 
! ------------------------------------------------------------

      open(unit=18,file='input',status='old',action='read')

      read(18,*) programa
      read(18,*) sysname
      read(18,*) choosatom
      read(18,*) nspin 
      read(18,*) choosedeg
      read(18,*) chooseae1
      read(18,*) chooseae2
      read(18,*) factorx,factory,factorz

      IF (choosedeg=='s') THEN 
       deg=1;lmax=0
       ELSEIF (choosedeg=='px') THEN 
       deg=1;lmax=1
       ELSEIF (choosedeg=='py') THEN 
       deg=2;lmax=1
       ELSEIF (choosedeg=='pz') THEN 
       deg=3;lmax=1
       ELSEIF (choosedeg=='dz2') THEN 
       deg=1;lmax=2
       ELSEIF (choosedeg=='dxz') THEN 
       deg=2;lmax=2
       ELSEIF (choosedeg=='dyz') THEN 
       deg=3;lmax=2
       ELSEIF (choosedeg=='dx2-y2') THEN 
       deg=4;lmax=2
       ELSEIF (choosedeg=='dxy') THEN 
       deg=5;lmax=2
      ENDIF
      
      IF(chooseae2.EQ.chooseae1) THEN
      nsp=1 
      ELSE
      nsp=2
      ENDIF

     open(unit=6,file="Results.dat",status='new',action='write')
     open(unit=11,file="log.file",status='new',action='write')

      IF (programa.eq."Siesta".or.programa.eq."siesta" .or.programa.eq."SIESTA" ) THEN 
         IF (nsp.eq.1 .and. nspin.eq.1)  then 
            programa="SIESTA"
         ELSEIF (nsp.eq.1 .and. nspin.eq.2.and.choosedeg .ne.'s')  then
            write(11,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            write(11,*)"Both all electron components are equal "
            write(11,*)"while your calculation is spin polarized: CHECK THIS INCONSISTENCY"
            write(11,*)"THE PROGRAM WILL ABORT EXECUTION..."
            write(11,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            stop
         ELSEIF (nsp.eq.1 .and. nspin.eq.2.and.choosedeg .eq.'s')  then
             programa="SIESTA_spin2"
!For relativistic pseudopotentials and to preserve the consistency 
!with the siesta calculation the second component is chosen. 
         ELSEIF (nsp.eq.2 .and.nspin.eq.1)  then
            programa="SIESTA_spin1"
         ELSEIF (nsp.eq.2 .and.nspin.eq.2)  then
            programa="SIESTA_spin"
         ENDIF
      ELSEIF (programa=="Abinit".or.programa=="abinit" .or. programa=="ABINIT")  then
         IF(nsp.eq.1 .and. nspin.eq.1)  then  
          programa="ABINIT" 
         ELSE
          write(11,*)"SORRY BUT FOR ABINIT, SPIN POLARIZATION IS NOT IMPLEMENTED YET"
          STOP 
         ENDIF
      ELSE 
      write(11,*)'VARIABLE PROGRAM UNKNOWN: CHECK INPUT FILE'
      STOP
      ENDIF

     SELECT CASE (programa)
     
     CASE ("SIESTA")
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae1,choosatom,sysname,integral,potential,1)

        write(6,*)'================================================='
        write(6,100)sysname,choosedeg,integral
        100 format (T20,'RESULT FOR SYSTEM:         ',A10,/// &
                   1X,'and angular momentum   ',A6,///&
                   1X,'Normalization of the radial and spherical parts give      ',F10.8,///&
                   1X,'The energy is:')
        write(6,*)potential,"Ry"
        write(6,*)'================================================='

     CASE ("SIESTA_spin")
        write(6,*) "YOUR SYSTEM IS SPIN POLARIZED"
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae1,choosatom,sysname,integral,potential,1)

        write(6,*)'================================================='
        write(6,101)sysname,choosedeg,integral
        101 format (T20,'RESULTS FOR SYSTEM:        ',A10,/// &
                   1X,'and angular momentum       ',A6,///&
                   1X,'Normalization of the radial and spherical parts give      ',F10.8,///&
                   1X,'The energy FOR SPIN 1 IS:')
        write(6,*)potential,"Ry"
        write(6,*)'================================================='
        write(11,*)deg,lmax,chooseae2,choosatom,choosedeg 
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae2,choosatom,sysname,integral,potential,2)

        write(6,*)'================================================='
        write(6,*)'The energy FOR SPIN 2 IS:'
        write(6,*)potential,"Ry"
        write(6,*)'================================================='

     CASE ("SIESTA_spin1")
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae2,choosatom,sysname,integral,potential,1)

        write(6,*)'================================================='
        write(6,103)sysname,choosedeg,integral
        103 format (T20,'Result for system         ',A10,/// &
                   1X,'and angular momentum    ',A6,///&
                   1X,'Normalization of the radial and spherical parts give      ',F10.8,///&
                   1X,'The energy IS:')
        write(6,*)potential,"Ry"
        write(6,*)'================================================='

     CASE ("SIESTA_spin2")
        write(6,*) "YOUR SYSTEM IS SPIN POLARIZED"
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae1,choosatom,sysname,integral,potential,1)

        write(6,*)'================================================='
        write(6,102)sysname,choosedeg,integral
        102 format (T20,'RESULTS FOR SYSTEM:        ',A10,/// &
                   1X,'and angular momentum       ',A6,///&
                   1X,'Normalization of the radial and spherical parts give      ',F10.8,///&
                   1X,'The energy FOR SPIN 1 IS:')
        write(6,*)potential,"Ry"
        write(6,*)'================================================='
        
        call siespart(deg,lmax,factorx,factory,factorz,choosedeg, &
      & chooseae2,choosatom,sysname,integral,potential,2)

        write(6,*)'================================================='
        write(6,*)'The energy FOR SPIN 2 IS:'
        write(6,*)potential,"Ry"
        write(6,*)'================================================='

    CASE ("ABINIT")
       call abinitpart(deg,lmax,factorx,factory,factorz,choosedeg,&
     & chooseae1,choosatom,sysname,integral,potential)

        write(6,*)'================================================='
        write(6,107)sysname,choosedeg,integral
        107 format (T20,'Results for system         ',A10,/// &
                   1X,'and angular momentum   ',A6,///&
                   1X,'Normalization of the radial and spherical parts give      ',F10.8,///&
                   1X,'The energy is:')
        write(6,*)potential,"Ry"
        write(6,*)'================================================='

    CASE default
         write(11,*)"Program is not recognized: ",programa
    STOP
   
    END SELECT
 
    close(6)
    close(11)

END PROGRAM PRINCIPAL
