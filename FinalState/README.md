## Final State Shifts:

**Notes by Sandra:**

**1. Creating a pseudoatom with a hole in the core:**

Current versions of atom include a line in the input.f file like this:

```
cc   Example of manual fiddling
cc   Ionize an electron from 1s..
!   zo(4)= 3.d0
!   zcore = zcore - 1.d0
```

Uncomment the last two lines to, for example, change the occupation of the p level (2p6 ---> 2p5).

**2. Create the new pseudoatom (and test it...).**

**3. In siesta:** Add a new species with Z+1, for example for oxygen in:

```
SystemLabel   oxalico

NumberOfAtoms  16

NumberOfSpecies 4
%block ChemicalSpeciesLabel
  1  6 C
  2  1 H
  3  8 O
  4  9 O_hole
%endblock ChemicalSpeciesLabel

######################
NetCharge 1
#####################
```

**4. Add a net charge "NetCharge 1".** This has different "issues" cell size, metal VS insulator, etc. I can send you the thesis chapter if you are interested.

**5. Voilá. It is necessary to compare the previous result with another to have the shift.**

